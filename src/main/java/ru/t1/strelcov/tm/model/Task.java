package ru.t1.strelcov.tm.model;

import ru.t1.strelcov.tm.enumerated.Status;

import java.util.UUID;

import static ru.t1.strelcov.tm.enumerated.Status.*;

public class Task {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private Status status = NOT_STARTED;

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return id + " : " + name;
    }

}
