package ru.t1.strelcov.tm.api.service;

import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project add(String name, String description);

    void add(Project project);

    void clear();

    void remove(Project project);

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByName(String oldName, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeStatusById(String id, Status status);

    Project changeStatusByName(String oldName, Status status);

    Project changeStatusByIndex(Integer index, Status status);

}
